## Alfresco Tesseract Shell Script
Scans pdf's and images for text using tesseract ocr and re-indexes these documents into alfresco.
All from an external shell script.

## Requirements
 - Documents must already exist in alfresco.
 - Use the alfresco bulk importer: <http://localhost:8080/alfresco/service/bulkfsimport>
 - You can disable all rules involving pdf and image ocr processing.

## How to use?

## Dev Notes

### To Access Solr Web Interface in Alfresco
<http://docs.alfresco.com/5.0/tasks/ssl-protect-solrwebapp.html>
Import the following certificate into your browser using password "alfresco" when prompted
/opt/alfresco-community/alf_data/keystore/browser.p12

### Alfresco documents look like this in solr
```javascript
"docs": [
      {
        "id": "_DEFAULT_!800000000000000b!800000000000a4f3",
        "_version_": 0,
        "DBID": 42227
      },
      {
        "id": "_DEFAULT_!8000000000000042!80000000000010f0",
        "_version_": 0,
        "DBID": 4336
      }
]
```

### Access DB

```bash
psql -h /tmp -Ualfresco alfresco
# specify admin password
```
