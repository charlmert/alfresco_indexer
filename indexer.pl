#!/usr/bin/perl

use DBI;
use strict;
use File::Basename;
use POSIX 'strftime';

# Database Config
my $driver = "Pg"; 
my $host = "/tmp";
my $userid = "alfresco";
my $password = "tOk7jnz";
my $database = "alfresco";
my $dsn = "DBI:$driver:database=$database;host=$host";

# Alfresco Config
my $alfresco_storage_path = '/opt/alfresco-community/alf_data/contentstore';
my $alfresco_temp_path = '/tmp';

# Log Date Format
my $date_format = '%Y-%m-%d %H:%M:%S';


my $dbh = DBI->connect($dsn, $userid, $password ) or die $DBI::errstr;

my $query = <<'END_QUERY';
	SELECT n.id AS "Node ID",
		n.store_id AS "Store ID",
		round(u.content_size/1024/1024,2) AS "Size (MB)",
		n.uuid AS "Document ID (UUID)",
		n.audit_creator AS "Creator",
		n.audit_created AS "Creation Date",
		n.audit_modifier AS "Modifier",
		n.audit_modified AS "Modification Date",
		p1.string_value AS "Document Name",
		u.content_url AS "Location"
	FROM alf_node AS n,
		alf_node_properties AS p,
		alf_node_properties AS p1,
		alf_node_properties AS p2,
		alf_namespace AS ns,
		alf_qname AS q,
		alf_qname AS q1,
		alf_content_data AS d,
		alf_content_url AS u
	WHERE n.id=p.node_id
		AND ns.id=q.ns_id
		AND p.qname_id=q.id
		AND p.long_value=d.id
		AND d.content_url_id=u.id
		AND p1.node_id=n.id
		AND p1.qname_id IN (SELECT id FROM alf_qname WHERE local_name='name')
		AND n.type_qname_id=q1.id
		AND p2.node_id=n.id
		AND p2.qname_id IN (SELECT id FROM alf_qname WHERE local_name='name')
		AND q1.local_name='content'
		AND (
		p2.string_value ILIKE '%.pdf'
		OR p2.string_value ILIKE '%.png'
		OR p2.string_value ILIKE '%.tif'
		OR p2.string_value ILIKE '%.tiff'
		OR p2.string_value ILIKE '%.jpg'
		OR p2.string_value ILIKE '%.jpeg'
		);
END_QUERY

my $sth = $dbh->prepare($query);
$sth->execute() or die $DBI::errstr;
print "Number of rows found :" + $sth->rows;
while (my @row = $sth->fetchrow_array()) {
	my ($node_id, $store_id, $size_mb, $document_id, $creator, $creation_date, $modifier, $modification_date, $document_name, $location) = @row;
	my $pass = 1;

	$location =~ s/^\s+|\s+$//g;
	$location =~ s/store:\/\///g;
  $location = "$alfresco_storage_path/$location";
	my $filename_hash = basename($location);

	my ($filename, $ext) = split(/\./, $document_name);

	my $filename_hash_new = $filename_hash;
	$filename_hash_new =~ s/\.bin/.$ext/g;

	# trim whitespace
	$document_name =~ s/^\s+|\s+$//g;

	my $result = '';
  if ($document_name =~ /\.png$/i) {
		$result = `echo cp $location $alfresco_temp_path/$filename_hash_new`;
		my $date = strftime $date_format, localtime;

		if ($? != 0) {
			$pass = 0;
			print $date . ',INDEXER,SOLR,ERROR,' . $result . "\n";
		}
		print $date . ',INDEXER,SOLR,OK,' . $result . "\n";
		#print "Location: $location\n";
  }

  if ($document_name =~ /\.pdf$/i) {
		#print "NodeID: $node_id, Document Name: $document_name, Location: $location\n";
  }

}
$sth->finish();


